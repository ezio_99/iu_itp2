#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

template<class P>
double dot_product(P begin1, P end1, P begin2) {
    P current1 = begin1;
    P current2 = begin2;
    double result = 0;
    while (current1 != end1) {
        result += *current1++ * *current2++;
    }
    return result;
}

struct PointInEuclideanCoordinates {
    double x, y;

    explicit PointInEuclideanCoordinates(double x, double y) : x(x), y(y) {}

    friend ostream &operator<<(ostream &out, const PointInEuclideanCoordinates &p) {
        out << "x = " << p.x << "; y = " << p.y;
        return out;
    }
};

class PointInPolarCoordinates {
private:
    double radius, angle;

    void fix_angle() {
        int quotient = static_cast<int>(abs(angle) / 2);
        if (angle > 0) {
            angle -= 2 * quotient;
        } else if (angle < 0) {
            angle += 2 * (quotient + 1);
        }
    }

public:
    explicit PointInPolarCoordinates(double r, double fi) : radius(abs(r)), angle(fi) {
        if (r < 0) {
            throw invalid_argument("Radius should be positive");
        }
        fix_angle();
    }

    explicit PointInPolarCoordinates(const PointInEuclideanCoordinates &p) {
        radius = sqrt(pow(p.x, 2) + pow(p.y, 2));
        angle = asin(p.y / radius);
        fix_angle();
    }

    PointInPolarCoordinates(const PointInPolarCoordinates &p) {
        radius = p.get_radius();
        angle = p.get_angle();
    }

    [[nodiscard]] double get_radius() const {
        return radius;
    }

    [[nodiscard]] double get_angle() const {
        return angle;
    }

    void set_radius(double r) {
        if (r < 0) {
            throw invalid_argument("Radius should be positive");
        }
        radius = abs(r);
    }

    void set_angle(double fi) {
        angle = fi;
        fix_angle();
    }

    friend ostream &operator<<(ostream &out, const PointInPolarCoordinates &p) {
        out << "radius = " << p.radius << "; angle = " << p.angle;
        return out;
    }

    PointInPolarCoordinates &operator=(const PointInPolarCoordinates &p) {
        if (this != &p) {
            radius = p.get_radius();
            angle = p.get_angle();
        }
        return *this;
    }

    PointInPolarCoordinates operator+(const PointInPolarCoordinates &p) {
        return PointInPolarCoordinates(radius + p.get_radius(), angle + p.get_angle());
    }

    PointInPolarCoordinates operator-(const PointInPolarCoordinates &p) {
        return PointInPolarCoordinates(radius - p.get_radius(), angle - p.get_angle());
    }

    PointInEuclideanCoordinates to_euclidean() {
        return PointInEuclideanCoordinates(radius * cos(angle), radius * sin(angle));
    }

    static PointInPolarCoordinates from_euclidean(double x, double y) {
        return PointInPolarCoordinates(PointInEuclideanCoordinates(x, y));
    }
};

template<class P>
double distance(P begin1, P end1, P begin2) {
    P current1 = begin1;
    P current2 = begin2;
    double result = 0;
    while (current1 != end1) {
        result += pow(*current1++ - *current2++, 2);
    }
    return sqrt(result);
}

int main() {
    vector<int> a = {1, 2, 3};
    vector<int> b = {3, 2, 1};
    cout << dot_product(begin(a), end(a), begin(b)) << endl;

    int arr1[] = {1, 2, 3, 4};
    int arr2[] = {3, 2, 1, 4};
    cout << dot_product(begin(arr1), end(arr1), begin(arr2)) << endl;

    cout.precision(2);
    cout << fixed;

    PointInPolarCoordinates P(10, 1);
    cout << P << endl;
    P.set_angle(20);
    P.set_radius(20);
    cout << P << endl;
    cout << P.to_euclidean() << endl;

    PointInEuclideanCoordinates E(5, 5);

    P = PointInPolarCoordinates(E);
    cout << P << endl;

    cout << distance(begin(a), end(a), begin(b));

    return 0;
}
