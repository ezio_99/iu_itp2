#include <iostream>
#include <string>

using namespace std;

template<class T>
class Pair1 {
private:
    T _first, _second;

public:
    explicit Pair1<T>(const T a, const T b) : _first(std::move(a)), _second(std::move(b)) {}

    [[nodiscard]] T first() const {
        return _first;
    }

    [[nodiscard]] T second() const {
        return _second;
    }
};

template<class T, class U>
class Pair {
private:
    T _first;
    U _second;
public:
    explicit Pair<T, U>(const T a, const U b) : _first(std::move(a)), _second(std::move(b)) {}

    [[nodiscard]] T first() const {
        return _first;
    }

    [[nodiscard]] U second() const {
        return _second;
    }
};

template<class T>
class StringValuePair : public Pair<string, T> {
public:
    StringValuePair<T>(const string& a, const T b) : Pair<string, T>(a, b) {}
};

void f1() {
    Pair1<int> p1(6, 9);
    std::cout << "Pair: " << p1.first() << ' ' << p1.second() << '\n';
    const Pair1<double> p2(3.4, 7.8);
    std::cout << "Pair: " << p2.first() << ' ' << p2.second() << '\n';
}

void f2() {
    Pair<int, double> p1(6, 7.8);
    std::cout << "Pair: " << p1.first() << ' ' << p1.second() << '\n';
    const Pair<double, int> p2(3.4, 5);
    std::cout << "Pair: " << p2.first() << ' ' << p2.second() << '\n';
}

void f3() {
    StringValuePair<int> svp("Amazing", 7);
    std::cout << "Pair: " << svp.first() << ' ' << svp.second() << '\n';
}

int main() {
    f1();

    return 0;
}
