#include <iostream>

using namespace std;

int main() {
    int n, m;
    cin >> n >> m;
    int **array = new int *[n];

    for (int i = 0; i < n; i++) {
        array[i] = new int[m];
    }

    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            cin >> *((*(array + i)) + j);
        }
    }

    for (int i = 0; i < n - 1; ++i) {
        for (int j = 0; j < m - 1; ++j) {
            cout << *(*(array + i) + j) << " ";
        }
        cout << *(*(array + i) + m - 1) << endl;
    }
    for (int j = 0; j < m - 1; ++j) {
        cout << *(*(array + n - 1) + j) << " ";
    }
    cout << *(*(array + n - 1) + m - 1);

    for (int i = 0; i < n; ++i) {
        delete[] array[i];
    }
    delete[] array;

    return 0;
}
