#include <iostream>
#include <string>
#include <cmath>

using namespace std;

class Fraction {
private:
    static int gcd(int a, int b) {
        a = abs(a);
        b = abs(b);
        while (a != 0 && b != 0) {
            if (a > b) {
                a %= b;
            } else {
                b %= a;
            }
        }
        return a + b;
    }

    void init_from_double(double n) {
        string str = to_string(n);
        int i = str.find('.');
        denominator = pow(10, str.length() - i - 1);

        string s = str.substr(0, i);
        s += str.substr(i + 1, str.length() - i - 1);
        numerator = stoi(s);

        simplify();
    }

protected:
    int numerator = 1;
    int denominator = 1;

public:
    Fraction() = default;

    Fraction(int n, int d) : numerator(n), denominator(d) {
        simplify();
    }

    explicit Fraction(int n) : numerator(n), denominator(1) {}

    explicit Fraction(double n) {
        init_from_double(n);
    }

    Fraction &operator=(int i) {
        numerator = i;
        denominator = 1;
        return *this;
    }

    Fraction &operator=(double i) {
        init_from_double(i);
        return *this;
    }

    // copy
    Fraction(Fraction &f) {
        numerator = f.getNumerator();
        denominator = f.getDenominator();
    }

    // copy assignment
    Fraction &operator=(Fraction const &f) {
        if (this != &f) {
            numerator = f.getNumerator();
            denominator = f.getDenominator();
        }
        return *this;
    }

    // move
    Fraction(Fraction &&f) noexcept {
        numerator = f.numerator;
        denominator = f.denominator;
    }

    // move assignment
    Fraction &operator=(Fraction &&f) noexcept {
        numerator = f.numerator;
        denominator = f.denominator;
        return *this;
    }

    friend ostream &operator<<(ostream &out, const Fraction &f) {
        if (f.getNumerator() == 0) {
            out << 0;
        } else {
            int whole = f.getNumerator() / f.getDenominator();
            if (whole == 0) {
                out << f.numerator << "/" << f.denominator;
            } else {
                out << whole;
                int remainder = f.getNumerator() % f.getDenominator();
                if (remainder != 0) {
                    out << ".";
                    int p = whole > 0 ? 1 : -1;
                    cout << remainder * p << "/" << f.getDenominator();
                }
            }
        }
        return out;
    }

    friend istream &operator>>(istream &in, Fraction &f) {
        string str;
        in >> str;
        int slash_index = str.find('/');
        int dot_index = str.find('.');
        if (slash_index != -1) {
            f.denominator = stoi(str.substr(slash_index + 1, str.length() - slash_index - 1));
            if (dot_index != -1) {  // case 1.2/3
                int whole = stoi(str.substr(0, dot_index));
                f.numerator = whole * f.denominator + stoi(str.substr(dot_index + 1, slash_index - dot_index - 1));
            } else {  // case 2/3
                f.numerator = stoi(str.substr(0, slash_index));
            }
        } else {
            if (dot_index != -1) {  // case 1.5
                f.init_from_double(stod(str));
            } else {  // case 1
                f.numerator = stoi(str);
                f.denominator = 1;
            }
        }

        f.simplify();
        return in;
    }

    [[nodiscard]] int getNumerator() const {
        return numerator;
    }

    [[nodiscard]] int getDenominator() const {
        return denominator;
    }

    void simplify() {
        int gcd = Fraction::gcd(numerator, denominator);
        numerator /= gcd;
        denominator /= gcd;
    }

    Fraction operator-() const {
        return Fraction(-numerator, denominator);
    }

    Fraction operator+(const Fraction &f) const {
        return Fraction(numerator * f.getDenominator() + f.numerator * denominator, denominator * f.getDenominator());
    }

    Fraction operator-(Fraction &f) const {
        return Fraction(numerator * f.getDenominator() - f.numerator * denominator, denominator * f.getDenominator());
    }

    Fraction operator*(const Fraction &f) const {
        return Fraction(numerator * f.getNumerator(), denominator * f.getDenominator());
    }

    Fraction operator/(Fraction &f) const {
        return Fraction(numerator * f.getDenominator(), denominator * f.getNumerator());
    }

    Fraction operator+(int i) const {
        return Fraction(numerator + i * denominator, denominator);
    }

    Fraction operator-(int i) const {
        return Fraction(numerator - i * denominator, denominator);
    }

    Fraction operator*(int i) const {
        return Fraction(numerator * i, denominator);
    }

    Fraction operator/(int i) const {
        return Fraction(numerator, denominator * i);
    }

    Fraction operator+(double i) const {
        Fraction r(i);
        return *this + r;
    }

    Fraction operator-(double i) const {
        Fraction r(i);
        return *this - r;
    }

    Fraction operator*(double i) const {
        Fraction r(i);
        return *this * r;
    }

    Fraction operator/(double i) const {
        Fraction r(i);
        return *this / r;
    }

    bool operator==(Fraction &f) const {
        return numerator * f.getDenominator() == denominator * f.getNumerator();
    }

    bool operator!=(Fraction &f) const {
        return numerator * f.getDenominator() != denominator * f.getNumerator();
    }

    bool operator<(Fraction &f) const {
        return numerator * f.getDenominator() < denominator * f.getNumerator();
    }

    bool operator<=(Fraction &f) const {
        return numerator * f.getDenominator() <= denominator * f.getNumerator();
    }

    bool operator>(Fraction &f) const {
        return numerator * f.getDenominator() > denominator * f.getNumerator();
    }

    bool operator>=(Fraction &f) const {
        return numerator * f.getDenominator() >= denominator * f.getNumerator();
    }

    explicit operator int() const {
        return numerator / denominator;
    }

    explicit operator double() const {
        return double(numerator) / denominator;
    }
};