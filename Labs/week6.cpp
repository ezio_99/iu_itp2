#include <iostream>
#include "Fraction.cpp"

using namespace std;

void f1() {
    int n;
    cin >> n;
    Fraction F(n);
    cout << F;
}

void f2() {
    Fraction F;
    cin >> F;
    cout << (int) F;
}

void f3() {
    double n;
    cin >> n;
    Fraction F(n);
    cout << F;
}

void f4() {
    Fraction F;
    cin >> F;
    cout.precision(2);
    cout << fixed << (double) F;
}

void f5() {
    Fraction F1, F2;
    cin >> F1 >> F2;
    Fraction F = F1 + F2;
    cout << F;
}

void f6() {
    Fraction F1, F2;
    cin >> F1 >> F2;
    Fraction F = F1 - F2;
    cout << F;
}

void f7() {
    Fraction F1, F2;
    cin >> F1 >> F2;
    Fraction F = F1 * F2;
    cout << F;
}

void f8() {
    Fraction F1, F2;
    cin >> F1 >> F2;
    Fraction F = F1 / F2;
    cout << F;
}

void f9() {
    Fraction F;
    cin >> F;
    F = -F;
    cout << F;
}

void f10() {
    Fraction F1, F2;
    cin >> F1 >> F2;
    cout << F1 << " is ";
    if (F1 > F2) {
        cout << "greater than";
    } else if (F1 < F2) {
        cout << "less than";
    } else {
        cout << "equal to";
    }
    cout << " " << F2;
}

int main() {
    int choice;
    bool exit = false;
    while (!exit) {
        cin >> choice;
        cin.ignore();
        switch (choice) {
            case 1:
                f1();
                break;
            case 2:
                f2();
                break;
            case 3:
                f3();
                break;
            case 4:
                f4();
                break;
            case 5:
                f5();
                break;
            case 6:
                f6();
                break;
            case 7:
                f7();
                break;
            case 8:
                f8();
                break;
            case 9:
                f9();
                break;
            case 10:
                f10();
                break;
            default:
                exit = true;
                break;
        }
        cout << endl;
    }

    return 0;
}
