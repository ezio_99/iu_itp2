#include <iostream>

using namespace std;

void F(int *A, int *B, int N) {
    int max = 1;
    int zeros = 0;

    for (int i = 0; i < N; ++i) {
        if (A[i] != 0) {
            max *= A[i];
        } else {
            zeros++;
        }
        if (zeros == 2) {
            break;
        }
    }

    if (zeros == 2) {
        for (int i = 0; i < N; ++i) {
            B[i] = 0;
        }
    } else if (zeros == 1) {
        for (int i = 0; i < N; ++i) {
            if (A[i] == 0) {
                B[i] = max;
            } else {
                B[i] = 0;
            }
        }
    } else {
        for (int i = 0; i < N; ++i) {
            B[i] = max / A[i];
        }
    }

    for (int i = 0; i < N - 1; i++) {
        cout << B[i] << " ";
    }
    cout << B[N-1];
}

int main() {
    int N;
    cin >> N;
    if (N == 1) {
        cout << 0;
    } else {
        int *A = new int[N];
        for (int i = 0; i < N; i++) {
            cin >> A[i];
        }
        int *B = new int[N];
        F(A, B, N);
    }
    return 0;
}