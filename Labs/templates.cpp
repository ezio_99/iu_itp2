#include <iostream>
#include "Fraction.cpp"
#include "../Matrices/Matrices.cpp"

using namespace std;

int main() {
    int n, m;

    cin >> n >> m;
    Matrix<double> D1(n, m);
    cin >> D1;

    cin >> n >> m;
    Matrix<double> D2(n, m);
    cin >> D2;

    cin >> n >> m;
    Matrix<Fraction> F1(n, m);
    cin >> F1;

    cin >> n >> m;
    Matrix<Fraction> F2(n, m);
    cin >> F2;

    cout.precision(2);
    cout << fixed;

    Matrix<double> D = D1 + D2;
    cout << D << endl;

    Matrix<Fraction> F = F1 * F2;
    cout << F;

    return 0;
}
