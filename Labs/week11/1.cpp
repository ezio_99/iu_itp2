#include <iostream>
#include <vector>
#include <type_traits>

using namespace std;

struct LengthsNotEqualException : std::exception {
    [[nodiscard]] const char *what() const noexcept override {
        return "The lengths of two containers are not equal";
    }
};

template<typename P>
auto dot_product(P begin1, P end1, P begin2, P end2)
-> typename std::remove_reference<decltype(*begin1)>::type {
    if (end1 - begin1 != end2 - begin2) {
        throw LengthsNotEqualException();
    }
    P current1 = begin1;
    P current2 = begin2;
    using T = typename std::remove_reference<decltype(*begin1)>::type;
    T result = static_cast<T>(0);
    while (current1 != end1) {
        result += *current1++ * *current2++;
    }
    return result;
}

template<typename Container>
void fill_container(Container &container, int n) {
    auto back_inserter = std::back_insert_iterator<Container>(container);
    typename Container::value_type element;
    for (int i = 0; i < n; ++i) {
        cin >> element;
        back_inserter = element;
    }
}

int main() {
    int n;

    cin >> n;
    vector<int> a;
    a.reserve(n);
    fill_container(a, n);

    cin >> n;
    vector<int> b;
    b.reserve(n);
    fill_container(b, n);

    try {
        cout << dot_product(begin(a), end(a), begin(b), end(b));
    } catch (const LengthsNotEqualException &err) {
        cout << err.what();
    }

    return 0;
}
