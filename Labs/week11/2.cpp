#include <iostream>
#include <vector>
#include <cmath>
#include <type_traits>

using namespace std;

template<class P>
auto distance(P begin1, P end1, P begin2, P end2)
-> typename std::remove_reference<decltype(*begin1)>::type {
    if (end1 - begin1 != end2 - begin2) {
        throw std::invalid_argument("invalid_argument exception");
    }

    P current1 = begin1;
    P current2 = begin2;
    using T = typename std::remove_reference<decltype(*begin1)>::type;
    T result = static_cast<T>(0);
    bool is_different = false;
    while (current1 != end1) {
        if (*current1 != *current2) {
            is_different = true;
        }
        result += pow(*current1++ - *current2++, 2);
    }

    if (!is_different) {
        throw std::domain_error("domain_error exception");
    }

    result = sqrt(result);

    if (result >= 100) {
        throw std::length_error("length_error exception");
    }

    return result;
}

template<typename Container>
void fill_container(Container &container, int n) {
    auto back_inserter = std::back_insert_iterator<Container>(container);
    typename Container::value_type element;
    for (int i = 0; i < n; ++i) {
        cin >> element;
        back_inserter = element;
    }
}

int main() {
    int n;

    cin >> n;
    vector<int> a;
    a.reserve(n);
    fill_container(a, n);

    cin >> n;
    vector<int> b;
    b.reserve(n);
    fill_container(b, n);

    try {
        cout << distance(begin(a), end(a), begin(b), end(b));
    } catch (const std::invalid_argument &err) {
        cout << err.what();
    } catch (const std::domain_error &err) {
        cout << err.what();
    } catch (const std::length_error &err) {
        cout << err.what();
    }

    return 0;
}
