#include <iostream>
#include "Matrices.cpp"

using namespace std;

void f1() {
    int n, m;
    cin >> n >> m;
    Matrix<int> A(n, m);
    cin >> A;

    cin >> n >> m;
    Matrix<int> B(n, m);
    cin >> B;

    cin >> n >> m;
    Matrix<int> C(n, m);
    cin >> C;

    Matrix<int> D = A + B;
    cout << D << endl;

    Matrix<int> E = B - A;
    cout << E << endl;

    Matrix<int> F = C * A;
    cout << F << endl;

    Matrix<int> G = A.transpose();
    cout << G;
}

void f2() {
    int n;
    cin >> n;
    SquareMatrix<int> A(n);
    cin >> A;

    cin >> n;
    SquareMatrix<int> B(n);
    cin >> B;

    cin >> n;
    SquareMatrix<int> C(n);
    cin >> C;

    SquareMatrix<int> D = A + B;
    cout << D << endl;

    SquareMatrix<int> E = B - A;
    cout << E << endl;

    SquareMatrix<int> F = C * A;
    cout << F << endl;

    SquareMatrix<int> G = A.transpose();
    cout << G;
}

void f3() {
    int n;
    cin >> n;
    SquareMatrix<int> A(n);
    cin >> A;

    IdentityMatrix<int> I(3);
    cout << I << endl;

    EliminationMatrix<int> E21(A, 2, 1);
    cout << E21 << endl;

    A = E21 * A;
    cout << A << endl;

    PermutationMatrix<int> P21(A, 2, 1);
    cout << P21 << endl;

    A = P21 * A;
    cout << A;
}

int main() {
    SquareMatrix<int> A(2);
    SquareMatrix<int> B(2);
    for (int i = 1; i <= 2; i++) {
        for (int j = 1; j <= 2; j++) {
            A.set(i, j, i+j);
            B.set(i, j, i * i + j * j);
        }
    }
    cout << A << endl << endl << B << endl << endl << endl;

    PermutationMatrix P(A, 2, 1);
    Matrix<int> C(2, 2);
    C = P * (A + B);

    cout << C;

    return 0;
}
