#include <iostream>
#include <cstdio>
#include <cmath>
#include <vector>
#include <fstream>
#include <string>

using namespace std;

// if you will compile and run this file on PC with Windows
// please specify path to your dataset and gnuplot executable
#ifdef WIN32
    const char *DATASET_FILENAME = "C:\\dataset.dat";
    const char *GNUPLOT_NAME = "C:\\Program Files (x86)\\gnuplot\\bin\\pgnuplot.exe";
#else
    const char *DATASET_FILENAME = "/data/work/iptii/Matrices/gnuplot/dataset.dat";
    const char *GNUPLOT_NAME = "gnuplot -persist";
#endif

const int DEGREE = 2;
const int NPOINTS = 400;
const int MIN = -20;
const int MAX = 20;

template<class T>
class Matrix {
private:
    int rows = -1;
    int cols = -1;
    T **values = nullptr;

    void remove_values() {
        if (values != nullptr) {
            for (int i = 0; i < rows; i++) {
                delete[] *(values + i);
            }
            delete[] values;
        }
    }

    void create_values() {
        T **m = new T *[rows];
        for (int i = 0; i < rows; ++i) {
            m[i] = new T[cols];
        }
        this->values = m;
    }

    T calculate_element_for_multiplication(int r, int c, const Matrix<T> &m) const {
        T sum = static_cast<T>(0);
        for (int i = 1; i <= cols; i++) {
            sum = sum + get(r, i) * m.get(i, c);
        }
        return sum;
    }

protected:
    void set_size(int r, int c) {
        if (rows != r || cols != c) {
            remove_values();
            rows = r;
            cols = c;
            create_values();
        }
    }

    void copy_from(const Matrix<T> &m) {
        set_size(m.row_count(), m.col_count());
        for (int i = 1; i <= this->rows; i++) {
            for (int j = 1; j <= this->cols; j++) {
                set(i, j, m.get(i, j));
            }
        }
    }

public:
    Matrix<T>(int r, int c) : rows(r), cols(c) {
        if (r < 0 || c < 0) {
            throw std::out_of_range("Dimension must be positive");
        }
        create_values();
    }

    ~Matrix<T>() {
        remove_values();
    }

    friend ostream &operator<<(ostream &out, Matrix<T> &m) {
        out << m.get(1, 1);
        for (int i = 2; i <= m.col_count(); i++) {
            out << " " << m.get(1, i);
        }
        for (int i = 2; i <= m.row_count(); i++) {
            out << endl << m.get(i, 1);
            for (int j = 2; j <= m.col_count(); j++) {
                out << " " << m.get(i, j);
            }
        }
        return out;
    }

    friend istream &operator>>(istream &in, Matrix<T> &m) {
        for (int i = 1; i <= m.row_count(); i++) {
            for (int j = 1; j <= m.col_count(); j++) {
                T el;
                in >> el;
                m.set(i, j, el);
            }
        }
        return in;
    }

    void set(int r, int c, T v) {
        if (r < 0 || r > rows || c < 0 || c > cols) {
            throw std::out_of_range("Out of range");
        }
        values[r - 1][c - 1] = v;
    }

    T get(int r, int c) const {
        if (r < 0 || r > rows || c < 0 || c > cols) {
            throw std::out_of_range("Out of range");
        }
        return values[r - 1][c - 1];
    }

    int col_count() const {
        return cols;
    }

    int row_count() const {
        return rows;
    }

    Matrix<T>(const Matrix<T> &m) {
        copy_from(m);
    }

    Matrix<T>(Matrix<T> &&m) noexcept: rows(m.row_count()), cols(m.col_count()) {
        std::swap(values, m.values);
    }

    Matrix<T> &operator=(Matrix<T> &&m) noexcept {
        std::swap(values, m.values);
        std::swap(rows, m.rows);
        std::swap(cols, m.cols);
        return *this;
    }

    Matrix<T> operator+(const Matrix<T> &m) const {
        Matrix<T> result(rows, cols);
        for (int i = 1; i <= rows; i++) {
            for (int j = 1; j <= cols; j++) {
                result.set(i, j, this->get(i, j) + m.get(i, j));
            }
        }
        return result;
    }

    Matrix<T> operator-(const Matrix<T> &m) const {
        Matrix<T> result(rows, cols);
        for (int i = 1; i <= rows; i++) {
            for (int j = 1; j <= cols; j++) {
                result.set(i, j, this->get(i, j) - m.get(i, j));
            }
        }
        return result;
    }

    Matrix<T> operator*(const Matrix<T> &m) const {
        Matrix<T> result(rows, m.col_count());
        for (int i = 1; i <= rows; i++) {
            for (int j = 1; j <= m.col_count(); j++) {
                result.set(i, j, calculate_element_for_multiplication(i, j, m));
            }
        }
        return result;
    }

    Matrix<T> transpose() const {
        Matrix<T> result(cols, rows);
        for (int i = 1; i <= rows; i++) {
            for (int j = 1; j <= cols; j++) {
                result.set(j, i, get(i, j));
            }
        }
        return result;
    }
};

template<class T>
class IdentityMatrix : public Matrix<T> {
public:
    explicit IdentityMatrix<T>(int n) : Matrix<T>(n, n) {
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                this->set(i, j, static_cast<T>(0));
            }
        }
        for (int i = 1; i <= n; i++) {
            this->set(i, i, static_cast<T>(1));
        }
    }
};

template<class T>
class EliminationMatrix : public IdentityMatrix<T> {
public:
    EliminationMatrix<T>(Matrix<T> &m, int r, int c) : IdentityMatrix<T>(m.row_count()) {
        this->set(r, c, -m.get(r, c) / m.get(c, c));
    }
};

template<class T>
class PermutationMatrix : public IdentityMatrix<T> {
public:
    PermutationMatrix<T>(Matrix<T> &m, int r, int c) : IdentityMatrix<T>(m.row_count()) {
        this->set(r, r, static_cast<T>(0));
        this->set(c, c, static_cast<T>(0));
        this->set(r, c, static_cast<T>(1));
        this->set(c, r, static_cast<T>(1));
    }
};

template<class T>
class JordanGaussMatrix : public Matrix<T> {
public:
    explicit JordanGaussMatrix<T>(Matrix<T> m) : Matrix<T>(m.row_count(), m.col_count() * 2) {
        for (int i = 1; i <= m.row_count(); i++) {
            for (int j = 1; j <= m.col_count(); j++) {
                this->set(i, j, m.get(i, j));
                this->set(i, j + m.row_count(), static_cast<T>(0));
            }
        }
        for (int i = 1; i <= m.row_count(); i++) {
            this->set(i, i + m.row_count(), static_cast<T>(1));
        }
    }
};

template<class T>
class ColumnVector : public Matrix<T> {
public:
    explicit ColumnVector<T>(int n) : Matrix<T>(n, 1) {}

    explicit ColumnVector<T>(const Matrix<T> &m) : Matrix<T>(m.row_count(), 1) {
        if (m.col_count() > 1) {
            throw std::invalid_argument("Matrix must have 1 column");
        }
        for (int r = 1; r <= m.row_count(); ++r) {
            set(r, m.get(r, 1));
        }
    }

    void set(int r, T v) {
        Matrix<T>::set(r, 1, v);
    }

    T get(int r) const {
        return Matrix<T>::get(r, 1);
    }
};

template<typename T>
int find_pivot_row(const Matrix<T> &A, int col) {
    T max = abs(A.get(col, col));
    int index = col;
    for (int i = col + 1; i <= A.row_count(); i++) {
        if (max < abs(A.get(i, col))) {
            max = abs(A.get(i, col));
            index = i;
        }
    }
    return index;
}

template<typename T>
void to_upper_triangular(Matrix<T> &A) {
    for (int c = 1; c < A.row_count(); c++) {
        int pivot_row = find_pivot_row(A, c);
        if (pivot_row != c) {
            Matrix<T> P = PermutationMatrix<T>(A, pivot_row, c);
        }
        for (int r = c + 1; r <= A.row_count(); r++) {
            Matrix<T> E = EliminationMatrix<T>(A, r, c);
            A = E * A;
        }
    }
}

template<typename T>
void to_lower_triangular(Matrix<T> &A) {
    for (int c = A.row_count(); c > 1; c--) {
        for (int r = c - 1; r > 0; r--) {
            Matrix<T> E = EliminationMatrix<T>(A, r, c);
            A = E * A;
        }
    }
}

template<typename T>
void diagonal_normalization(Matrix<T> &A) {
    for (int r = 1; r <= A.row_count(); ++r) {
        T pivot = A.get(r, r);
        for (int c = 1; c <= A.col_count(); ++c) {
            if (A.get(r, c) != T(0)) {
                A.set(r, c, A.get(r, c) / pivot + numeric_limits<T>::epsilon());
            }
        }
    }
}

template<typename T>
Matrix<T> get_inverse(const Matrix<T> &A) {
    if (A.row_count() != A.col_count()) {
        throw std::invalid_argument("Matrix must be square");
    }
    Matrix<T> J = JordanGaussMatrix<T>(A);
    to_upper_triangular(J);
    to_lower_triangular(J);
    diagonal_normalization(J);

    Matrix<T> A_inverse(A.row_count(), A.col_count());

    for (int r = 1; r <= A.row_count(); ++r) {
        for (int c = 1; c <= A.row_count(); ++c) {
            A_inverse.set(r, c, J.get(r, c + A.row_count()));
        }
    }

    return A_inverse;
}

template<typename T>
void fill_A(Matrix<T> &A, const ColumnVector<T> &t) {
    for (int r = 1; r <= A.row_count(); ++r) {
        A.set(r, 1, 1);
    }
    for (int r = 1; r <= A.row_count(); ++r) {
        for (int c = 2; c <= A.col_count(); ++c) {
            A.set(r, c, pow(t.get(r), c - 1));
        }
    }
}

template<typename T>
struct Point {
    T x, y;
};

vector<Point<double>> get_dataset() {
    ifstream dataset_file;
    dataset_file.open(DATASET_FILENAME);
    if (!dataset_file) {
        throw std::runtime_error("Bad dataset file");
    }
    vector<Point<double>> dataset;
    double x, y;
    while (dataset_file >> x) {
        dataset_file >> y;
        dataset.push_back({x, y});
    }
    dataset_file.close();
    return dataset;
}

ColumnVector<double> get_equation(vector<Point<double>> dataset, int degree) {
    ColumnVector<double> t(dataset.size());
    ColumnVector<double> b(dataset.size());
    for (int i = 1; i <= dataset.size(); ++i) {
        t.set(i, dataset[i - 1].x);
        b.set(i, dataset[i - 1].y);
    }
    Matrix<double> A(dataset.size(), degree + 1);
    fill_A(A, t);
    Matrix<double> A_T = A.transpose();
    Matrix<double> A_T_A = A_T * A;
    Matrix<double> A_T_A_1 = get_inverse(A_T_A);
    Matrix<double> A_T_b = A_T * b;
    Matrix<double> x_ = A_T_A_1 * A_T_b;
    return ColumnVector<double>(x_);
}

string get_equation_as_string(const ColumnVector<double> &equation) {
    string command = "y = " + to_string(equation.get(1));

    if (equation.row_count() > 1) {
        if (equation.get(2) > 0) {
            command += " + " + to_string(equation.get(2)) + "x";
        } else if (equation.get(2) < 0) {
            string tmp = to_string(equation.get(2));
            command += " - " + tmp.substr(1, tmp.length() - 1) + "x";
        }
    }

    if (equation.row_count() > 2) {
        for (int i = 3; i <= equation.row_count(); ++i) {
            if (equation.get(i) > 0) {
                command += " + " + to_string(equation.get(i)) + "x^" + to_string(i - 1);
            } else if (equation.get(i) < 0) {
                string tmp = to_string(equation.get(i));
                command += " - " + tmp.substr(1, tmp.length() - 1) + "x^" + to_string(i - 1);
            }
        }
    }

    return command;
}

double get_y(const ColumnVector<double> &equation, double x) {
    double y = equation.get(1);
    for (int r = 2; r <= equation.row_count(); ++r) {
        y += equation.get(r) * pow(x, r - 1);
    }
    return y;
}

void plot_graph() {
    #ifdef WIN32
        FILE *pipe = _popen(GNUPLOT_NAME, "w");
    #else
        FILE *pipe = popen(GNUPLOT_NAME, "w");
    #endif

    if (pipe != nullptr) {
        auto dataset = get_dataset();
        auto equation = get_equation(dataset, DEGREE);

        for (const auto &p : dataset) {
            fprintf(pipe, "%s%f%s%f%s\n", "set object rectangle at first ", p.x, ", ", p.y,
                    " size char 1, char 0.6 fillcolor rgb 'blue' fillstyle solid border lt 2 lw 2");
        }

        const double step = static_cast<double>(MAX - MIN) / NPOINTS;
        string equation_as_string = get_equation_as_string(equation);
        string command = "plot '-' using 1:2 title '" + equation_as_string + "' with lines";
        cout << equation_as_string << endl;
        fprintf(pipe, "%s\n", command.c_str());

        for (int i = 0; i < NPOINTS + 1; ++i) {
            double x = MIN + i * step;
            double y = get_y(equation, x);
            fprintf(pipe, "%f\t%f\n", x, y);
        }

        fprintf(pipe, "%s\n", "e");
        fflush(pipe);

        cin.clear();
        cin.ignore(cin.rdbuf()->in_avail());
        cin.get();

        #ifdef WIN32
            _pclose(pipe);
        #else
            pclose(pipe);
        #endif
    } else {
        cout << "Could not open pipe" << endl;
    }
}

int main() {
    plot_graph();

    return 0;
}
