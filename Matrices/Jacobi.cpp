#include <iostream>
#include "Matrices.cpp"

using namespace std;

template<typename T>
double compute_epsilon(const ColumnVector<T> &x, const ColumnVector<T> &x_new) {
    auto new_eps = static_cast<ColumnVector<T>>(x_new - x);
    return new_eps.get_length();
}

template<typename T>
bool check_diagonal_predominance(const Matrix<T> &A) {
    for (int c = 1; c <= A.col_count(); ++c) {
        T sum = static_cast<T>(0);
        for (int r = 1; r <= A.row_count(); ++r) {
            if (c != r) {
                sum += A.get(r, c);
            }
        }
        if (abs(sum) >= abs(A.get(c, c))) return false;
    }
    return true;
}

template<typename T>
void compute_alpha_beta(Matrix<T> &alpha, ColumnVector<T> &beta) {
    T pivot;
    for (int r = 1; r <= alpha.row_count(); ++r) {
        pivot = alpha.get(r, r);
        for (int c = 1; c <= alpha.col_count(); ++c) {
            alpha.set(r, c, -alpha.get(r, c) / pivot);
        }
        beta.set(r, beta.get(r) / pivot);
        alpha.set(r, r, static_cast<T>(0));
    }
}

int main() {
    int n_A;
    cin >> n_A;
    Matrix<double> A(n_A, n_A);
    cin >> A;

    int n_b;
    cin >> n_b;
    ColumnVector<double> b(n_b);
    cin >> b;

    double eps;
    cin >> eps;

    cout.precision(4);
    cout << fixed;

    if (!check_diagonal_predominance(A)) {
        cout << "The method is not applicable!";
        return 0;
    }

    auto alpha = A;
    auto beta = b;
    compute_alpha_beta(alpha, beta);

    cout << "alpha:" << endl << alpha << endl;
    cout << "beta:" << endl << beta << endl;

    auto x = beta;
    cout << "x(0):" << endl << x;

    double new_eps;
    int i = 0;
    ColumnVector<double> x_new(b.row_count());
    while (true) {
        x_new = static_cast<ColumnVector<double>>(beta + alpha * x);
        ++i;
        new_eps = compute_epsilon(x, x_new);
        cout << endl << "e: " << new_eps;
        cout << endl << "x(" << i << "):" << endl << x_new;
        if (new_eps <= eps) break;
        x = x_new;
    }

    return 0;
}
