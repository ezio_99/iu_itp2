#include <iostream>
#include <complex>
#include <cmath>
#include <iterator>

using namespace std;

const double PI = 3.1415926536;

unsigned int Prepare(unsigned int x, int log2n) {
    unsigned int n = 0;
    for (int i = 0; i < log2n; i++) {
        n <<= 1u;
        n |= (x & 1u);
        x >>= 1u;
    }
    return n;
}

template<typename Iterator>
void fft(Iterator a, Iterator b, int log2n) {
    int n = pow(2, log2n);
    const complex<double> IMAGINARY(0, 1);

    for (unsigned int i = 0; i < n; ++i) {
        b[Prepare(i, log2n)] = a[i];
    }

    for (int s = 1; s <= log2n; ++s) {
        int m = pow(2, s);
        complex<double> wm = exp(IMAGINARY * (-2 * PI / m));
        for (int k = 0; k <= n - 1; k += m) {
            complex<double> w(1, 0);
            for (int j = 0; j <= m / 2 - 1; ++j) {
                complex<double> t = w * b[k + j + m / 2];
                complex<double> u = b[k + j];
                b[k + j] = u + t;
                b[k + j + m / 2] = u - t;
                w *= wm;
            }
        }
    }
}

int main() {
    const int N = 8;
    complex<double> a[N];
    for (auto &i : a) {
        cin >> i;
        getchar();
    }
    complex<double> b[N];
    fft(a, b, log2(N));

    cout << b[0];
    for (int i = 1; i < N; ++i) {
        cout << endl << b[i];
    }
}
