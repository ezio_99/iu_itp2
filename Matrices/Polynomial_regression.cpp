#include <iostream>
#include <cmath>
#include <limits>
#include "Matrices.cpp"

using namespace std;

template<typename T>
int find_pivot_row(const Matrix<T> &A, int col) {
    T max = abs(A.get(col, col));
    int index = col;
    for (int i = col + 1; i <= A.row_count(); i++) {
        if (max < abs(A.get(i, col))) {
            max = abs(A.get(i, col));
            index = i;
        }
    }
    return index;
}

template<typename T>
void to_upper_triangular(Matrix<T> &A) {
    for (int c = 1; c < A.row_count(); c++) {
        int pivot_row = find_pivot_row(A, c);
        if (pivot_row != c) {
            Matrix<T> P = PermutationMatrix<T>(A, pivot_row, c);
        }
        for (int r = c + 1; r <= A.row_count(); r++) {
            Matrix<T> E = EliminationMatrix<T>(A, r, c);
            A = E * A;
        }
    }
}

template<typename T>
void to_lower_triangular(Matrix<T> &A) {
    for (int c = A.row_count(); c > 1; c--) {
        for (int r = c - 1; r > 0; r--) {
            Matrix<T> E = EliminationMatrix<T>(A, r, c);
            A = E * A;
        }
    }
}

template<typename T>
void diagonal_normalization(Matrix<T> &A) {
    for (int r = 1; r <= A.row_count(); ++r) {
        T pivot = A.get(r, r);
        for (int c = 1; c <= A.col_count(); ++c) {
            if (A.get(r, c) != T(0)) {
                A.set(r, c, A.get(r, c) / pivot + numeric_limits<T>::epsilon());
            }
        }
    }
}

template<typename T>
Matrix<T> get_inverse(const Matrix<T> &A) {
    if (A.row_count() != A.col_count()) {
        throw std::invalid_argument("Matrix must be square");
    }
    Matrix<T> J = JordanGaussMatrix<T>(A);
    to_upper_triangular(J);
    to_lower_triangular(J);
    diagonal_normalization(J);

    Matrix<T> A_inverse(A.row_count(), A.col_count());

    for (int r = 1; r <= A.row_count(); ++r) {
        for (int c = 1; c <= A.row_count(); ++c) {
            A_inverse.set(r, c, J.get(r, c + A.row_count()));
        }
    }

    return A_inverse;
}

template<typename T>
void fill_A(Matrix<T> &A, const ColumnVector<T> &t) {
    for (int r = 1; r <= A.row_count(); ++r) {
        A.set(r, 1, 1);
    }
    for (int r = 1; r <= A.row_count(); ++r) {
        for (int c = 2; c <= A.col_count(); ++c) {
            A.set(r, c, pow(t.get(r), c - 1));
        }
    }
}

int main() {
    int m, n;
    cin >> m;
    ColumnVector<double> t(m);
    ColumnVector<double> b(m);
    for (int tmp, i = 1; i <= m; ++i) {
        cin >> tmp;
        t.set(i, tmp);
        cin >> tmp;
        b.set(i, tmp);
    }
    cin >> n;

    cout.precision(2);
    cout << fixed;

    Matrix<double> A(m, n + 1);
    fill_A(A, t);
    cout << "A:" << endl << A << endl;

    Matrix<double> A_T = A.transpose();
    Matrix<double> A_T_A = A_T * A;
    cout << "A_T*A:" << endl << A_T_A << endl;

    Matrix<double> A_T_A_1 = get_inverse(A_T_A);
    cout << "(A_T*A)^-1:" << endl << A_T_A_1 << endl;

    Matrix<double> A_T_b = A_T * b;
    cout << "A_T*b:" << endl << A_T_b << endl;

    Matrix<double> x_ = A_T_A_1 * A_T_b;
    cout << "x~:" << endl << x_;

    return 0;
}
