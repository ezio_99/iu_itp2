#include <iostream>
#include "Matrices.cpp"

using namespace std;

template<class T>
T abs(T a) {
    return a >= 0 ? a : -a;
}

template<class T>
T calculate_determinant_for_upper_triangular_matrix(SquareMatrix<T> &A) {
    T result = 1;
    for (int i = 1; i <= A.row_count(); i++) {
        result *= A.get(i, i);
    }
    return result;
}

template<class T>
int find_pivot_row(Matrix<T> &A, int col) {
    T max = abs(A.get(col, col));
    int index = col;
    for (int i = col + 1; i <= A.row_count(); i++) {
        if (max < abs(A.get(i, col))) {
            max = abs(A.get(i, col));
            index = i;
        }
    }
    return index;
}

template<class T>
int to_upper_triangular_with_log(Matrix<T> &A) {
    int step = 1;
    for (int c = 1; c < A.row_count(); c++) {
        int pivot_row = find_pivot_row(A, c);
        if (pivot_row != c) {
            A.swap_rows(pivot_row, c);
            cout << "step #" << step << ": permutation" << endl << A << endl;
        }
        for (int r = c + 1; r <= A.row_count(); r++) {
            Matrix<double> E = EliminationMatrix<double>(A, r, c);
            A = E * A;
            cout << "step #" << step++ << ": elimination" << endl << A << endl;
        }
    }

    return step;
}

template<class T>
void to_lower_triangular_with_log(Matrix<T> &A, int step) {
    for (int c = A.row_count(); c > 1; c--) {
        for (int r = c - 1; r > 0; r--) {
            Matrix<double> E = EliminationMatrix<double>(A, r, c);
            A = E * A;
            cout << "step #" << step++ << ": elimination" << endl << A << endl;
        }
    }
}

template<class T>
void diagonal_normalization(Matrix<T> &A) {
    for (int r = 1; r <= A.row_count(); r++) {
        T d = A.get(r, r);
        for (int c = 1; c <= A.col_count(); c++) {
            A.set(r, c, A.get(r, c) / d);
        }
    }
}

template<class T>
void print_inverse(Matrix<T> &A) {
    int n = A.row_count();
    cout << A.get(1, n + 1);
    for (int c = 2; c <= n; c++) {
        cout << " " << A.get(1, n + c);
    }
    for (int r = 2; r <= n; r++) {
        cout << endl << A.get(r, 1 + n);
        for (int c = 2; c <= n; c++) {
            cout << " " << A.get(r, c + n);
        }
    }
}

template<class T>
int to_upper_triangular(Matrix<T> &A, Matrix<T> &b) {
    int step = 1;
    for (int c = 1; c < A.row_count(); c++) {
        int pivot_row = find_pivot_row(A, c);
        if (pivot_row != c) {
            A.swap_rows(pivot_row, c);
            b.swap_rows(pivot_row, c);
            cout << "step #" << step << ": permutation" << endl << A << endl << b << endl;
        }
        for (int r = c + 1; r <= A.row_count(); r++) {
            Matrix<double> E = EliminationMatrix<double>(A, r, c);
            A = E * A;
            b = E * b;
            cout << "step #" << step++ << ": elimination" << endl << A << endl << b << endl;
        }
    }

    return step;
}

template<class T>
void to_lower_triangular_with_log(Matrix<T> &A, Matrix<T> &b, int step) {
    for (int c = A.row_count(); c > 1; c--) {
        for (int r = c - 1; r > 0; r--) {
            Matrix<double> E = EliminationMatrix<double>(A, r, c);
            A = E * A;
            b = E * b;
            cout << "step #" << step++ << ": elimination" << endl << A << endl << b << endl;
        }
    }
}

template<class T>
void diagonal_normalization(Matrix<T> &A, Matrix<T> &b) {
    for (int r = 1; r <= A.row_count(); r++) {
        if (b.get(r, 1) != (T) 0) {
            b.set(r, 1, b.get(r, 1) / A.get(r, r));
        }
        A.set(r, r, (T) 1);
    }
}

void f1() {
    int n;
    cin >> n;
    SquareMatrix<double> A = SquareMatrix<double>(n);
    cin >> A;
    cout.precision(2);
    cout << fixed;
    to_upper_triangular_with_log(A);
    cout << "result:" << endl;
    cout << calculate_determinant_for_upper_triangular_matrix(A);
}

void f2() {
    int n;
    cin >> n;
    SquareMatrix<double> M(n);
    cin >> M;
    Matrix<double> A = JordanGaussMatrix<double>(M);

    cout.precision(2);
    cout << fixed;

    cout << "step #0: Augmented Matrix" << endl;
    cout << A << endl;

    cout << "Direct way:" << endl;

    int step = to_upper_triangular_with_log(A);

    cout << "Way back:" << endl;
    to_lower_triangular_with_log(A, step);

    diagonal_normalization(A);
    cout << "Diagonal normalization:" << endl << A << endl << "result:" << endl;
    print_inverse(A);
}

void f3() {
    int n;
    cin >> n;
    SquareMatrix<double> A(n);
    cin >> A;
    cin >> n;
    ColumnVector<double> b(n);
    cin >> b;

    cout.precision(2);
    cout << fixed;

    cout << "step #0:" << endl;
    cout << A << endl << b << endl;
    int step = to_upper_triangular(A, b);

    cout << "Way back:" << endl;
    to_lower_triangular_with_log(A, b, step);

    diagonal_normalization(A, b);
    cout << "Diagonal normalization:" << endl << A << endl << b << endl << "result:" << endl << b;
}

int main() {
    f3();

    return 0;
}
