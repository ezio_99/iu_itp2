#include <iostream>
#include "Matrices.cpp"

using namespace std;

template<typename T>
bool check_diagonal_predominance(const Matrix<T> &A) {
    for (int c = 1; c <= A.col_count(); ++c) {
        T sum = static_cast<T>(0);
        for (int r = 1; r <= A.row_count(); ++r) {
            if (c != r) {
                sum += A.get(r, c);
            }
        }
        if (abs(sum) >= abs(A.get(c, c))) return false;
    }
    return true;
}

template<typename T>
double compute_epsilon(const ColumnVector<T> &x, const ColumnVector<T> &x_new) {
    auto new_eps = static_cast<ColumnVector<T>>(x_new - x);
    return new_eps.get_length();
}

template<typename T>
Matrix<T> extract_upper_part(const Matrix<T> &m) {
    Matrix<T> result(m.row_count(), m.col_count());
    for (int r = 1; r <= m.row_count(); ++r) {
        for (int c = 1; c <= m.col_count(); ++c) {
            result.set(r, c, r < c ? m.get(r, c) : static_cast<T>(0));
        }
    }
    return result;
}

template<typename T>
Matrix<T> extract_lower_part(const Matrix<T> &m) {
    Matrix<T> result(m.row_count(), m.col_count());
    for (int r = 1; r <= m.row_count(); ++r) {
        for (int c = 1; c <= m.col_count(); ++c) {
            result.set(r, c, r > c ? m.get(r, c) : static_cast<T>(0));
        }
    }
    return result;
}

template<typename T>
void compute_alpha_beta(Matrix<T> &alpha, ColumnVector<T> &beta) {
    T pivot;
    for (int r = 1; r <= alpha.row_count(); ++r) {
        pivot = alpha.get(r, r);
        for (int c = 1; c <= alpha.col_count(); ++c) {
            alpha.set(r, c, -alpha.get(r, c) / pivot);
        }
        beta.set(r, beta.get(r) / pivot);
        alpha.set(r, r, static_cast<T>(0));
    }
}

int main() {
    int n_A;
    cin >> n_A;
    Matrix<double> A(n_A, n_A);
    cin >> A;

    int n_b;
    cin >> n_b;
    ColumnVector<double> b(n_b);
    cin >> b;

    double eps;
    cin >> eps;

    cout.precision(4);
    cout << fixed;

    if (!check_diagonal_predominance(A)) {
        cout << "The method is not applicable!";
        return 0;
    }

    auto alpha = A;
    auto beta = b;
    compute_alpha_beta(alpha, beta);

    cout << "beta:" << endl << beta << endl;
    cout << "alpha:" << endl << alpha << endl;

    auto B = extract_lower_part(alpha);
    cout << "B:" << endl << B << endl;

    auto C = extract_upper_part(alpha);
    cout << "C:" << endl << C << endl;

    IdentityMatrix<double> I(n_A);
    auto I_B = I - B;
    cout << "I-B:" << endl << I_B << endl;

    auto I_B_1 = get_inverse(I - B);
    cout << "(I-B)^-1:" << endl << I_B_1 << endl;

    auto x = beta;
    cout << "x(0):" << endl << x;

    int i = 0;
    double new_eps;
    ColumnVector<double> x_new(x.row_count());
    while (true) {
        for (int j = 1; j <= x_new.row_count(); ++j) {
            double xj = beta.get(j);
            for (int c = j + 1; c <= alpha.col_count(); ++c) {
                xj += alpha.get(j, c) * x.get(c);
            }
            for (int c = 1; c < j; ++c) {
                xj += alpha.get(j, c) * x_new.get(c);
            }
            x_new.set(j, xj);
        }

        ++i;
        new_eps = compute_epsilon(x, x_new);
        cout << endl << "e: " << new_eps;
        cout << endl << "x(" << i << "):" << endl << x_new;
        if (new_eps <= eps) break;
        x = x_new;
    }

    return 0;
}
