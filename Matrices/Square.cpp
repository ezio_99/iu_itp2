template<class T>
class SquareMatrix : public Matrix<T> {
private:
    SquareMatrix<T>(const Matrix<T> &&m) noexcept : Matrix<T>(m.row_count(), m.col_count()) {
        this->values = m.values;
        m.values = nullptr;
    }

public:
    explicit SquareMatrix<T>(int n) : Matrix<T>(n, n) {}

    SquareMatrix<T>(const SquareMatrix &m) : Matrix<T>(m.row_count(), m.row_count()){
        this->copy_from(m);
    }

    SquareMatrix<T> &operator=(SquareMatrix<T> &&m) noexcept {
        std::swap(this->values, m.values);
        std::swap(this->rows, m.rows);
        std::swap(this->cols, m.cols);
        return *this;
    }

    SquareMatrix<T> &operator=(const SquareMatrix<T> m) const {
        if (this != &m) {
            this->copy_from(m);
        }
        return *this;
    }

    SquareMatrix<T> operator+(const SquareMatrix<T> m) const {
        return SquareMatrix<T>(Matrix<T>(*this) + m);
    }

    SquareMatrix<T> operator-(const SquareMatrix<T> m) const {
        return SquareMatrix<T>(*this - m);
    }

    SquareMatrix<T> operator*(const SquareMatrix<T> m) const {
        return SquareMatrix<T>(*this * m);
    }

    SquareMatrix<T> operator/(const SquareMatrix<T> m) const {
        return SquareMatrix<T>(*this / m);
    }

    SquareMatrix<T> transpose() const {
        return SquareMatrix<T>(Matrix<T>(*this).transpose());
    }
};
