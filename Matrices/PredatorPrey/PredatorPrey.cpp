#include <iostream>
#include <cmath>

using namespace std;

double V(double v0, double k0, double t, double a1, double a2, double b1, double b2) {
    return v0 * cos(sqrt(a1 * a2) * t) - k0 * sqrt(a2) * b1 / (b2 * sqrt(a1)) * sin(sqrt(a1 * a2) * t) + a2 / b2;
}

double K(double v0, double k0, double t, double a1, double a2, double b1, double b2) {
    return v0 * sqrt(a1) * b2 / (b1 * sqrt(a2)) * sin(sqrt(a1 * a2) * t) + k0 * cos(sqrt(a1 * a2) * t) + a1 / b1;
}

int main() {
    double v0, k0;
    cin >> v0 >> k0;
    double a1, b1, a2, b2;
    cin >> a1 >> b1 >> a2 >> b2;
    double T;
    cin >> T;
    int N;
    cin >> N;

    cout.precision(2);
    cout << fixed;

    cout << "t:" << endl;
    double step = T / N;

    cout << 0.0;
    for (int i = 1; i <= N; ++i) {
        cout << ' ' << step * i;
    }

    double v_original = v0;
    double k_original = k0;
    v0 -= a2 / b2;
    k0 -= a1 / b1;

    cout << endl << "v:" << endl << v_original;
    for (int i = 1; i <= N; ++i) {
        cout << ' ' << V(v0, k0, step * i, a1, a2, b1, b2);
    }

    cout << endl << "k:" << endl << k_original;
    for (int i = 1; i <= N; ++i) {
        cout << ' ' << K(v0, k0, step * i, a1, a2, b1, b2);
    }

    return 0;
}
