#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

// if you will compile and run this file on PC with Windows
// please specify path to your dataset and gnuplot executable
#ifdef WIN32
    const char *GNUPLOT_NAME = "C:\\Program Files (x86)\\gnuplot\\bin\\pgnuplot.exe";
#else
    const char *GNUPLOT_NAME = "gnuplot -persist";
#endif

double V(double v0, double k0, double t, double a1, double a2, double b1, double b2) {
    return v0 * cos(sqrt(a1 * a2) * t) - k0 * sqrt(a2) * b1 / (b2 * sqrt(a1)) * sin(sqrt(a1 * a2) * t) + a2 / b2;
}

double K(double v0, double k0, double t, double a1, double a2, double b1, double b2) {
    return v0 * sqrt(a1) * b2 / (b1 * sqrt(a2)) * sin(sqrt(a1 * a2) * t) + k0 * cos(sqrt(a1 * a2) * t) + a1 / b1;
}

int main() {
    double v0, k0;
    cin >> v0 >> k0;
    double a1, b1, a2, b2;
    cin >> a1 >> b1 >> a2 >> b2;
    double T;
    cin >> T;
    int N;
    cin >> N;

    #ifdef WIN32
        FILE *pipe = _popen(GNUPLOT_NAME, "w");
    #else
        FILE *pipe = popen(GNUPLOT_NAME, "w");
    #endif

    if (pipe != nullptr) {
        double step = T / N;

        v0 -= a2 / b2;
        k0 -= a1 / b1;

        fprintf(pipe, "%s\n%s\n%s\n",
                "set multiplot layout 1,2 title 'Predator-prey model' font ',14'",
                "set title 'Process in time'",
                "plot '-' using 1:2 title 'victims' with lines, '-' using 1:2 title 'killers' with lines"
        );

        vector<double> vs;
        vector<double> ks;

        double x;
        for (int i = 0; i <= N; ++i) {
            x = step * i;
            vs.push_back(V(v0, k0, x, a1, a2, b1, b2));
            fprintf(pipe, "%f\t%f\n", x, vs.back());
        }

        fprintf(pipe, "%s\n", "e");

        for (int i = 0; i <= N; ++i) {
            x = step * i;
            ks.push_back(K(v0, k0, x, a1, a2, b1, b2));
            fprintf(pipe, "%f\t%f\n", x, ks.back());
        }

        fprintf(pipe, "%s\n", "e");

        fprintf(pipe, "%s\n%s\n",
                "set title 'Predator-prey relation' offset 0,1",
                "plot '-' using 1:2 title 'relation' with lines"
        );

        for (int i = 0; i < vs.size(); ++i) {
            fprintf(pipe, "%f\t%f\n", vs[i], ks[i]);
        }

        fprintf(pipe, "%s\n", "unset multiplot");

        fflush(pipe);

        cin.clear();
        cin.ignore(cin.rdbuf()->in_avail());
        cin.get();

        #ifdef WIN32
            _pclose(pipe);
        #else
            pclose(pipe);
        #endif
    } else {
        cout << "Could not open pipe" << endl;
    }

    return 0;
}
