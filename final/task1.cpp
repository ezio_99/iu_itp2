#include <iostream>

int MaxDigit(int n) {
    int max = 0;
    while (n > 0) {
        int rem = n % 10;
        if (rem > max) {
            max = rem;
        }
        n /= 10;
    }
    return max;
}

int MinDigit(int n) {
    int min = 10;
    while (n > 0) {
        int rem = n % 10;
        if (rem < min) {
            min = rem;
        }
        n /= 10;
    }
    return min;
}

int calc(int a1, int k) {
    int result = a1;
    int last = a1;
    for (int i = 1; i < k; ++i) {
        result = last + MinDigit(last) * MaxDigit(last);
        last = result;
    }
    return result;
}

using namespace std;

int main() {
    int p, a1, k;
    cin >> p;
    for (int i = 0; i < p - 1; ++i) {
        cin >> a1 >> k;
        cout << calc(a1, k) << endl;
    }
    cin >> a1 >> k;
    cout << calc(a1, k);
    return 0;
}
