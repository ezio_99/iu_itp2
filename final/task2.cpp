#include <iostream>
#include <vector>

using namespace std;

int getDigitsCount(int num) {
    return to_string(num).length();
}

int pow(int a, int k) {
    int res = 1;
    for (int i = 0; i < k; ++i) {
        res *= a;
    }
    return res;
}

int main() {
    int n;
    cin >> n;

    vector<int> values;
    int m = getDigitsCount(n) - 1;
    int v, first;
    while (n > 0) {
        if (n > 9) {
            first = n / pow(10, m);
            v = first * pow(10, m);
            values.push_back(v);
        } else {
            v = n;
            values.push_back(v);
        }
        n -= v;
        --m;
    }

    cout << values.size() << endl;
    for (unsigned int i = values.size() - 1; i > 0; --i) {
        cout << values[i] << ' ';
    }
    cout << values[0];

    return 0;
}
